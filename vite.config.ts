import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"
import AutoImport from "unplugin-auto-import/vite"
import vuetify from "vite-plugin-vuetify"
import { resolve } from "path"
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      imports: ["vue"],
    }),
    vuetify({ autoImport: true, styles: { configFile: "src/settings.scss" } }),
  ],
  resolve: {
    alias: [
      {
        find: "@",
        replacement: resolve(__dirname, "src"),
      },
    ],
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@import "./src/assets/styles/variables.scss";`,
        javascriptEnabled: true,
      },
    },
  },
  // 打包配置
  build: {
    // 打包文件夹名称
    outDir: "dist",
    // 打包后去掉console语句
    minify: "terser",
    terserOptions: {
      compress: {
        drop_console: true,
        drop_debugger: true,
      },
    },
  },
  server: {
    host: "0.0.0.0",
    cors: true,
    open: true,
    proxy: {
      "/api": {
        target: "http://192.168.31.176:10909",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
})
