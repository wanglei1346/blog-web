import { defineStore } from "pinia"
export const useModelStore = defineStore("model", {
  state: () => ({
    searchFlag: false,
    loginFlag: false,
    registerFlag: false,
    forgetFlag: false,
  }),
  getters: {
    isMobile() {
      const clientWidth = document.documentElement.clientWidth
      if (clientWidth > 960) {
        return false
      }
      return true
    },
  },
  actions: {
    setSearchFlag(value: boolean) {
      this.searchFlag = value
    },
    setLoginFlag(value: boolean) {
      this.loginFlag = value
    },
    setRegisterFlag(value: boolean) {
      this.registerFlag = value
    },
    setForgetFlag(value: boolean) {
      this.forgetFlag = value
    },
  },
})
