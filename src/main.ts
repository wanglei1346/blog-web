import { createApp } from "vue"
import vuetify from "./plugins/vuetify"
import App from "./App.vue"
import router from "./router"
import { createPinia } from "pinia"
import piniaPluginPersist from "pinia-plugin-persist"
import "animate.css"
import "./assets/styles/index.scss"
import "./assets/styles/iconfont.css"
const pinia = createPinia()
pinia.use(piniaPluginPersist)
const app = createApp(App)
app.use(pinia).use(router).use(vuetify).mount("#app")
